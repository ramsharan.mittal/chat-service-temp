package co.seedwill.tech.chat.domain;

public enum MessageStatus {
    RECEIVED, DELIVERED
}
