package co.seedwill.tech.chat.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BotMessage {
	private String message;
	private String messageLabel;
	private NodeKey nodeKey;
	MessageValidation messageValidation;
	
	public void setMessage(String message) {
	  this.message = message;
	  this.messageLabel = message;
	}
	
	public abstract static class BotMessageBuilder<C extends BotMessage, B extends BotMessageBuilder<C,B>> {
	  private String message;
	    private String messageLabel;
	    private NodeKey nodeKey;
	    MessageValidation messageValidation;

	    public BotMessageBuilder message(String message) {
	        this.message = message;
	        this.messageLabel = message;
	        return this;
	    }
	    
	    public BotMessageBuilder messageLabel(String messageLabel) {
          this.messageLabel = messageLabel;
          return this;
      }
	    public BotMessageBuilder nodeKey(NodeKey nodeKey) {
          this.nodeKey = nodeKey;
          return this;
      }
	    public BotMessageBuilder messageValidation(MessageValidation messageValidation) {
          this.messageValidation = messageValidation;
          return this;
      }
      

	    
	}
}
