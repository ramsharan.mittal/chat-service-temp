package co.seedwill.tech.chat.domain;

public enum ChatBotOptionType {
  POPUP,
  MULTI_SELECT,
  SEARCHABLE,
  AUTO
}