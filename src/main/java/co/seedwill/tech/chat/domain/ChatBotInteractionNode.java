package co.seedwill.tech.chat.domain;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatBotInteractionNode {
	private NodeKey nodeKey;
	private ChatBotInputMessage chatBotInputMessage;
	private List<ChatBotOptionMessage> chatBotOptionMessageList;
	private ChatBotAnswerMessage chatBotAnswerMessage;
	private List<ChatBotInteractionNode> nextChatBotInteractionNodeList;
	
	public static ChatBotInteractionNode fromString(String str) throws JsonMappingException, JsonProcessingException {
	  return new ObjectMapper().readValue(str, ChatBotInteractionNode.class);
	}
}