package co.seedwill.tech.chat.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * A ChatbotMasterData.
 */
@Entity
@Table(name = "chatbot_master_data")
public class ChatbotMasterData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "master_key", unique = true)
    private String masterKey;

    @Column(name = "chat")
    private String chat;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ChatbotMasterData id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMasterKey() {
        return this.masterKey;
    }

    public ChatbotMasterData masterKey(String masterKey) {
        this.setMasterKey(masterKey);
        return this;
    }

    public void setMasterKey(String masterKey) {
        this.masterKey = masterKey;
    }

    public String getChat() {
        return this.chat;
    }

    public ChatbotMasterData chat(String chat) {
        this.setChat(chat);
        return this;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChatbotMasterData)) {
            return false;
        }
        return id != null && id.equals(((ChatbotMasterData) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChatbotMasterData{" +
            "id=" + getId() +
            ", masterKey='" + getMasterKey() + "'" +
            ", chat='" + getChat() + "'" +
            "}";
    }
}
