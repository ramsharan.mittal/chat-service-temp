package co.seedwill.tech.chat.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import co.seedwill.tech.chat.domain.ChatMessage;
import co.seedwill.tech.chat.domain.MessageStatus;

import java.util.List;

public interface ChatMessageRepository
        extends MongoRepository<ChatMessage, String> {

    long countBySenderIdAndRecipientIdAndStatus(
            String senderId, String recipientId, MessageStatus status);

    List<ChatMessage> findByChatId(String chatId);
}