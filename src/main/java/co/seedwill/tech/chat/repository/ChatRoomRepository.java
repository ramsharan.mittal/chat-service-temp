package co.seedwill.tech.chat.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import co.seedwill.tech.chat.domain.ChatRoom;

import java.util.Optional;

public interface ChatRoomRepository extends MongoRepository<ChatRoom, String> {
    Optional<ChatRoom> findBySenderIdAndRecipientId(String senderId, String recipientId);
}
