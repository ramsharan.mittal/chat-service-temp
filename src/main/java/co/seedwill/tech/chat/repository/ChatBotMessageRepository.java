package co.seedwill.tech.chat.repository;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import co.seedwill.tech.chat.domain.BotMessage;
import co.seedwill.tech.chat.domain.ChatBotInputMessage;
import co.seedwill.tech.chat.domain.ChatBotInteractionNode;
import co.seedwill.tech.chat.domain.ChatBotOptionMessage;
import co.seedwill.tech.chat.domain.ChatBotOptionType;
import co.seedwill.tech.chat.domain.NodeKey;

@Repository
public class ChatBotMessageRepository {
  private final Logger log = LoggerFactory.getLogger(ChatBotMessageRepository.class);

  public ChatBotInteractionNode save(ChatBotInteractionNode chatBotNodeMessage) {
    List<BotMessage> chatBotMessageListL1 = new ArrayList<BotMessage>();
    chatBotMessageListL1.add(BotMessage.builder().message("Hello, I’m Via.").build());
    chatBotMessageListL1.add(BotMessage.builder().message("Welcome to SeedWill").build());
    ChatBotInputMessage chatBotInputMessageL1 =
    ChatBotInputMessage.builder().messageList(chatBotMessageListL1).build();
    List<BotMessage> chatBotOptionMessageListL1 = new ArrayList<BotMessage>();
    chatBotOptionMessageListL1.add(BotMessage.builder()
        .message("Chat Here")
        .nodeKey(NodeKey.builder().key("fce2b857-2134-4658-968c-c4f5a43080ae").build())
        .build());
    chatBotOptionMessageListL1.add(BotMessage.builder()
        .message("Whatsapp")
        .nodeKey(NodeKey.builder().key("37a28ffc-800f-467b-b8a0-ae6949e49dae").build())
        .build());
    ChatBotOptionMessage chatBotOptionMessageL1 = ChatBotOptionMessage.builder().messageList(chatBotOptionMessageListL1)
        .messageType(ChatBotOptionType.POPUP).build();
    List<ChatBotOptionMessage> chatBotOptionMessageListListL1 = new ArrayList<ChatBotOptionMessage>();
    chatBotOptionMessageListListL1.add(chatBotOptionMessageL1);
    
    ChatBotInteractionNode chatBotNodeMessageL1 =
        ChatBotInteractionNode.builder()
        .nodeKey(NodeKey.builder().key("5e72deda-0e05-4ee0-982a-ce3dad3d9326").build())
        .chatBotInputMessage(chatBotInputMessageL1).chatBotOptionMessageList(chatBotOptionMessageListListL1).build();
    
    //===========//
    
//    List<BotMessage> chatBotOptionMessageListL1 = new ArrayList<BotMessage>();
//    chatBotOptionMessageListL1.add(BotMessage.builder()
//        .message("Property details")
//        .nodeKey(NodeKey.builder().key("fce2b857-2134-4658-968c-c4f5a43080ae").build())
//        .build());
//    chatBotOptionMessageListL1.add(BotMessage.builder()
//        .message("Loan assistance")
//        .nodeKey(NodeKey.builder().key("37a28ffc-800f-467b-b8a0-ae6949e49dae").build())
//        .build());
//    chatBotOptionMessageListL1.add(BotMessage.builder()
//        .message("Customer care")
//        .nodeKey(NodeKey.builder().key("fce2b857-2134-4658-968c-c4f5a43080ae").build())
//        .build());
//    chatBotOptionMessageListL1.add(BotMessage.builder()
//        .message("Call back")
//        .nodeKey(NodeKey.builder().key("37a28ffc-800f-467b-b8a0-ae6949e49dae").build())
//        .build());
//    ChatBotOptionMessage chatBotOptionMessageL1 = ChatBotOptionMessage.builder().messageList(chatBotOptionMessageListL1)
//        .messageType(ChatBotOptionType.POPUP).build();
//    List<BotMessage> chatBotOptionMessageListL2 = new ArrayList<BotMessage>();
//    chatBotOptionMessageListL2.add(BotMessage.builder()
//        .message("More...")
//        .nodeKey(NodeKey.builder().key("fce2b857-2134-4658-968c-c4f5a43080ae").build())
//        .build());
//    List<ChatBotOptionMessage> chatBotOptionMessageListListL1 = new ArrayList<ChatBotOptionMessage>();
//    chatBotOptionMessageListListL1.add(chatBotOptionMessageL1);
//    
//    ChatBotInteractionNode chatBotNodeMessageL1 =
//        ChatBotInteractionNode.builder()
//        .nodeKey(NodeKey.builder().key("5e72deda-0e05-4ee0-982a-ce3dad3d9326").build())
//        .chatBotOptionMessageList(chatBotOptionMessageListListL1).build();
    
    
    // String json = "{ \"color\" : \"Black\", \"type\" : \"BMW\" }";
    // ObjectMapper objectMapper = new ObjectMapper();
    // try {
    // chatBotNodeMessage = objectMapper.readValue(json, ChatBotNodeMessage.class);
    // } catch (JsonMappingException e) {
    // log.error("JsonMappingException " , e);
    // } catch (JsonProcessingException e) {
    // log.error("JsonProcessingException " , e);
    // }

    return chatBotNodeMessageL1;
  }

  /*
   * long countBySenderIdAndRecipientIdAndStatus( String senderId, String recipientId, MessageStatus
   * status);
   * 
   * List<ChatMessage> findByChatId(String chatId);
   */
}
