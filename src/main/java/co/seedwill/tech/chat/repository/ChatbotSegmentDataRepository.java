package co.seedwill.tech.chat.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.seedwill.tech.chat.domain.ChatbotSegmentData;

/**
 * Spring Data SQL repository for the ChatbotSegmentData entity.
 */
@Repository
public interface ChatbotSegmentDataRepository extends JpaRepository<ChatbotSegmentData, Long> {
  Optional<ChatbotSegmentData> findOneByMasterKey(String masterKey);
  
}
