package co.seedwill.tech.chat.repository;

import co.seedwill.tech.chat.domain.ChatbotMasterData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ChatbotMasterData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChatbotMasterDataRepository extends JpaRepository<ChatbotMasterData, Long> {}
