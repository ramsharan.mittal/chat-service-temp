package co.seedwill.tech.chat.service.impl;

import co.seedwill.tech.chat.domain.ChatBotInteractionNode;
import co.seedwill.tech.chat.domain.ChatbotMasterData;
import co.seedwill.tech.chat.domain.ChatbotSegmentData;
import co.seedwill.tech.chat.repository.ChatbotMasterDataRepository;
import co.seedwill.tech.chat.repository.ChatbotSegmentDataRepository;
import co.seedwill.tech.chat.service.ChatbotMasterDataService;
import co.seedwill.tech.chat.web.rest.errors.BadRequestAlertException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Service Implementation for managing {@link ChatbotMasterData}.
 */
@Service
@Transactional
public class ChatbotMasterDataServiceImpl implements ChatbotMasterDataService {

    private final Logger log = LoggerFactory.getLogger(ChatbotMasterDataServiceImpl.class);

    private final ChatbotMasterDataRepository chatbotMasterDataRepository;
    private final ChatbotSegmentDataRepository chatbotSegmentDataRepository;

    public ChatbotMasterDataServiceImpl(ChatbotMasterDataRepository chatbotMasterDataRepository, ChatbotSegmentDataRepository chatbotSegmentDataRepository) {
        this.chatbotMasterDataRepository = chatbotMasterDataRepository;
        this.chatbotSegmentDataRepository = chatbotSegmentDataRepository;
    }

    @Override
    public ChatbotMasterData save(ChatbotMasterData chatbotMasterData) {
        log.debug("Request to save ChatbotMasterData : {}", chatbotMasterData);
        
        try {
          ChatBotInteractionNode chatBotInteractionNode = new ObjectMapper()
              .readValue(
                  chatbotMasterData.getChat(), ChatBotInteractionNode.class);
          saveChatbotSegmentData(chatBotInteractionNode);
        } catch (JsonProcessingException e) {
          log.error("ChatbotMasterData json is not in right format", e);
          throw new BadRequestAlertException("ChatbotMasterData json is not in right format", "", "");
        }
        return chatbotMasterDataRepository.save(chatbotMasterData);
    }
    
    private void saveChatbotSegmentData(ChatBotInteractionNode chatBotInteractionNode) {
      List<ChatBotInteractionNode> nextChatBotInteractionNodeList = chatBotInteractionNode.getNextChatBotInteractionNodeList();
      if(nextChatBotInteractionNodeList != null) {
        for(ChatBotInteractionNode nextChatBotInteractionNode : nextChatBotInteractionNodeList) {
          saveChatbotSegmentData(nextChatBotInteractionNode);
        }
      }
      ChatBotInteractionNode chatBotInteractionNodeForSegmentPeristance = ChatBotInteractionNode
          .builder()
          .nodeKey(chatBotInteractionNode.getNodeKey())
          .chatBotInputMessage(chatBotInteractionNode.getChatBotInputMessage())
          .chatBotOptionMessageList(chatBotInteractionNode.getChatBotOptionMessageList())
          .chatBotAnswerMessage(chatBotInteractionNode.getChatBotAnswerMessage())
          .build();
      ChatbotSegmentData chatbotSegmentData;
      try {
        chatbotSegmentData = ChatbotSegmentData.builder()
        .masterKey(chatBotInteractionNode.getNodeKey().getKey())
        .chat(new ObjectMapper().writeValueAsString(chatBotInteractionNodeForSegmentPeristance))
        .build();
        chatbotSegmentDataRepository.save(chatbotSegmentData);
      } catch (JsonProcessingException e) {
        log.error("ChatbotSegmentrData in ChatbotMasterData json is not in right format", e);
        throw new BadRequestAlertException("ChatbotSegmentrData in ChatbotMasterData json is not in right format", "", "");
      }
    }

    @Override
    public Optional<ChatbotMasterData> partialUpdate(ChatbotMasterData chatbotMasterData) {
        log.debug("Request to partially update ChatbotMasterData : {}", chatbotMasterData);

        return chatbotMasterDataRepository
            .findById(chatbotMasterData.getId())
            .map(existingChatbotMasterData -> {
                if (chatbotMasterData.getMasterKey() != null) {
                    existingChatbotMasterData.setMasterKey(chatbotMasterData.getMasterKey());
                }
                if (chatbotMasterData.getChat() != null) {
                    existingChatbotMasterData.setChat(chatbotMasterData.getChat());
                }

                return existingChatbotMasterData;
            })
            .map(chatbotMasterDataRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChatbotMasterData> findAll() {
        log.debug("Request to get all ChatbotMasterData");
        return chatbotMasterDataRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ChatbotMasterData> findOne(Long id) {
        log.debug("Request to get ChatbotMasterData : {}", id);
        return chatbotMasterDataRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChatbotMasterData : {}", id);
        chatbotMasterDataRepository.deleteById(id);
    }
}
