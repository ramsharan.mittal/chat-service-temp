package co.seedwill.tech.chat.service.impl;

import co.seedwill.tech.chat.domain.Chat;
import co.seedwill.tech.chat.repository.ChatRepository;
import co.seedwill.tech.chat.service.ChatService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Chat}.
 */
@Service
@Transactional
public class ChatServiceImpl implements ChatService {

    private final Logger log = LoggerFactory.getLogger(ChatServiceImpl.class);

    private final ChatRepository chatRepository;

    public ChatServiceImpl(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }

    @Override
    public Chat save(Chat chat) {
        log.debug("Request to save Chat : {}", chat);
        return chatRepository.save(chat);
    }

    @Override
    public Optional<Chat> partialUpdate(Chat chat) {
        log.debug("Request to partially update Chat : {}", chat);

        return chatRepository
            .findById(chat.getId())
            .map(existingChat -> {
                if (chat.getMasterKey() != null) {
                    existingChat.setMasterKey(chat.getMasterKey());
                }
                if (chat.getChat() != null) {
                    existingChat.setChat(chat.getChat());
                }

                return existingChat;
            })
            .map(chatRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Chat> findAll() {
        log.debug("Request to get all Chats");
        return chatRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Chat> findOne(Long id) {
        log.debug("Request to get Chat : {}", id);
        return chatRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Chat : {}", id);
        chatRepository.deleteById(id);
    }
}
