package co.seedwill.tech.chat.service;

import co.seedwill.tech.chat.domain.Chat;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Chat}.
 */
public interface ChatService {
    /**
     * Save a chat.
     *
     * @param chat the entity to save.
     * @return the persisted entity.
     */
    Chat save(Chat chat);

    /**
     * Partially updates a chat.
     *
     * @param chat the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Chat> partialUpdate(Chat chat);

    /**
     * Get all the chats.
     *
     * @return the list of entities.
     */
    List<Chat> findAll();

    /**
     * Get the "id" chat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Chat> findOne(Long id);

    /**
     * Delete the "id" chat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
