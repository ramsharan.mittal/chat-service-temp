package co.seedwill.tech.chat.service.impl;

import co.seedwill.tech.chat.domain.ChatbotSegmentData;
import co.seedwill.tech.chat.repository.ChatbotSegmentDataRepository;
import co.seedwill.tech.chat.service.ChatbotSegmentDataService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ChatbotSegmentData}.
 */
@Service
@Transactional
public class ChatbotSegmentDataServiceImpl implements ChatbotSegmentDataService {

    private final Logger log = LoggerFactory.getLogger(ChatbotSegmentDataServiceImpl.class);

    private final ChatbotSegmentDataRepository chatbotSegmentDataRepository;

    public ChatbotSegmentDataServiceImpl(ChatbotSegmentDataRepository chatbotSegmentDataRepository) {
        this.chatbotSegmentDataRepository = chatbotSegmentDataRepository;
    }

    @Override
    public ChatbotSegmentData save(ChatbotSegmentData chatbotSegmentData) {
        log.debug("Request to save ChatbotSegmentData : {}", chatbotSegmentData);
        return chatbotSegmentDataRepository.save(chatbotSegmentData);
    }

    @Override
    public Optional<ChatbotSegmentData> partialUpdate(ChatbotSegmentData chatbotSegmentData) {
        log.debug("Request to partially update ChatbotSegmentData : {}", chatbotSegmentData);

        return chatbotSegmentDataRepository
            .findById(chatbotSegmentData.getId())
            .map(existingChatbotSegmentData -> {
                if (chatbotSegmentData.getMasterKey() != null) {
                    existingChatbotSegmentData.setMasterKey(chatbotSegmentData.getMasterKey());
                }
                if (chatbotSegmentData.getChat() != null) {
                    existingChatbotSegmentData.setChat(chatbotSegmentData.getChat());
                }

                return existingChatbotSegmentData;
            })
            .map(chatbotSegmentDataRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChatbotSegmentData> findAll() {
        log.debug("Request to get all ChatbotSegmentData");
        return chatbotSegmentDataRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ChatbotSegmentData> findOne(Long id) {
        log.debug("Request to get ChatbotSegmentData : {}", id);
        return chatbotSegmentDataRepository.findById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Optional<ChatbotSegmentData> findOneByMasterKey(String masterKey) {
        log.debug("Request to get ChatbotSegmentData : {}", masterKey);
        return chatbotSegmentDataRepository.findOneByMasterKey(masterKey);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChatbotSegmentData : {}", id);
        chatbotSegmentDataRepository.deleteById(id);
    }

  
}
