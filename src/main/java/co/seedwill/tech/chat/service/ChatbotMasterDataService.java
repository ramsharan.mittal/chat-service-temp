package co.seedwill.tech.chat.service;

import co.seedwill.tech.chat.domain.ChatbotMasterData;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ChatbotMasterData}.
 */
public interface ChatbotMasterDataService {
    /**
     * Save a chatbotMasterData.
     *
     * @param chatbotMasterData the entity to save.
     * @return the persisted entity.
     */
    ChatbotMasterData save(ChatbotMasterData chatbotMasterData);

    /**
     * Partially updates a chatbotMasterData.
     *
     * @param chatbotMasterData the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ChatbotMasterData> partialUpdate(ChatbotMasterData chatbotMasterData);

    /**
     * Get all the chatbotMasterData.
     *
     * @return the list of entities.
     */
    List<ChatbotMasterData> findAll();

    /**
     * Get the "id" chatbotMasterData.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChatbotMasterData> findOne(Long id);

    /**
     * Delete the "id" chatbotMasterData.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
