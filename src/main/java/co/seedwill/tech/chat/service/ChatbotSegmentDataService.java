package co.seedwill.tech.chat.service;

import co.seedwill.tech.chat.domain.ChatbotSegmentData;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ChatbotSegmentData}.
 */
public interface ChatbotSegmentDataService {
    /**
     * Save a chatbotSegmentData.
     *
     * @param chatbotSegmentData the entity to save.
     * @return the persisted entity.
     */
    ChatbotSegmentData save(ChatbotSegmentData chatbotSegmentData);

    /**
     * Partially updates a chatbotSegmentData.
     *
     * @param chatbotSegmentData the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ChatbotSegmentData> partialUpdate(ChatbotSegmentData chatbotSegmentData);

    /**
     * Get all the chatbotSegmentData.
     *
     * @return the list of entities.
     */
    List<ChatbotSegmentData> findAll();

    /**
     * Get the "id" chatbotSegmentData.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChatbotSegmentData> findOne(Long id);
    
    /**
     * Get the "masterKey" chatbotSegmentData.
     *
     * @param masterKey the masterKey of the entity.
     * @return the entity.
     */
    Optional<ChatbotSegmentData> findOneByMasterKey(String masterKey);

    /**
     * Delete the "id" chatbotSegmentData.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
