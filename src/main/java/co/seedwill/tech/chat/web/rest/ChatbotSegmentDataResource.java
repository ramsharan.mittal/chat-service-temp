package co.seedwill.tech.chat.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import co.seedwill.tech.chat.domain.ChatBotInteractionNode;
import co.seedwill.tech.chat.domain.ChatContext;
import co.seedwill.tech.chat.domain.ChatbotSegmentData;
import co.seedwill.tech.chat.repository.ChatbotSegmentDataRepository;
import co.seedwill.tech.chat.service.ChatbotSegmentDataService;
import co.seedwill.tech.chat.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link co.seedwill.tech.chat.domain.ChatbotSegmentData}.
 */
@RestController
@RequestMapping("/api")
public class ChatbotSegmentDataResource {

    private final Logger log = LoggerFactory.getLogger(ChatbotSegmentDataResource.class);

    private static final String ENTITY_NAME = "chatAppConeChatbotSegmentData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChatbotSegmentDataService chatbotSegmentDataService;

    private final ChatbotSegmentDataRepository chatbotSegmentDataRepository;

    public ChatbotSegmentDataResource(
        ChatbotSegmentDataService chatbotSegmentDataService,
        ChatbotSegmentDataRepository chatbotSegmentDataRepository
    ) {
        this.chatbotSegmentDataService = chatbotSegmentDataService;
        this.chatbotSegmentDataRepository = chatbotSegmentDataRepository;
    }

    /**
     * {@code POST  /chatbot-segment-data} : Create a new chatbotSegmentData.
     *
     * @param chatbotSegmentData the chatbotSegmentData to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chatbotSegmentData, or with status {@code 400 (Bad Request)} if the chatbotSegmentData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chatbot-segment-data")
    public ResponseEntity<ChatbotSegmentData> createChatbotSegmentData(@RequestBody ChatbotSegmentData chatbotSegmentData)
        throws URISyntaxException {
        log.debug("REST request to save ChatbotSegmentData : {}", chatbotSegmentData);
        if (chatbotSegmentData.getId() != null) {
            throw new BadRequestAlertException("A new chatbotSegmentData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChatbotSegmentData result = chatbotSegmentDataService.save(chatbotSegmentData);
        return ResponseEntity
            .created(new URI("/api/chatbot-segment-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chatbot-segment-data/:id} : Updates an existing chatbotSegmentData.
     *
     * @param id the id of the chatbotSegmentData to save.
     * @param chatbotSegmentData the chatbotSegmentData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chatbotSegmentData,
     * or with status {@code 400 (Bad Request)} if the chatbotSegmentData is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chatbotSegmentData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chatbot-segment-data/{id}")
    public ResponseEntity<ChatbotSegmentData> updateChatbotSegmentData(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChatbotSegmentData chatbotSegmentData
    ) throws URISyntaxException {
        log.debug("REST request to update ChatbotSegmentData : {}, {}", id, chatbotSegmentData);
        if (chatbotSegmentData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chatbotSegmentData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chatbotSegmentDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChatbotSegmentData result = chatbotSegmentDataService.save(chatbotSegmentData);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, chatbotSegmentData.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /chatbot-segment-data/:id} : Partial updates given fields of an existing chatbotSegmentData, field will ignore if it is null
     *
     * @param id the id of the chatbotSegmentData to save.
     * @param chatbotSegmentData the chatbotSegmentData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chatbotSegmentData,
     * or with status {@code 400 (Bad Request)} if the chatbotSegmentData is not valid,
     * or with status {@code 404 (Not Found)} if the chatbotSegmentData is not found,
     * or with status {@code 500 (Internal Server Error)} if the chatbotSegmentData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/chatbot-segment-data/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ChatbotSegmentData> partialUpdateChatbotSegmentData(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChatbotSegmentData chatbotSegmentData
    ) throws URISyntaxException {
        log.debug("REST request to partial update ChatbotSegmentData partially : {}, {}", id, chatbotSegmentData);
        if (chatbotSegmentData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chatbotSegmentData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chatbotSegmentDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChatbotSegmentData> result = chatbotSegmentDataService.partialUpdate(chatbotSegmentData);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, chatbotSegmentData.getId().toString())
        );
    }

    /**
     * {@code GET  /chatbot-segment-data} : get all the chatbotSegmentData.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chatbotSegmentData in body.
     */
    @GetMapping("/chatbot-segment-data")
    public List<ChatbotSegmentData> getAllChatbotSegmentData() {
        log.debug("REST request to get all ChatbotSegmentData");
        return chatbotSegmentDataService.findAll();
    }

    /**
     * {@code GET  /chatbot-segment-data/:id} : get the "id" chatbotSegmentData.
     *
     * @param id the id of the chatbotSegmentData to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chatbotSegmentData, or with status {@code 404 (Not Found)}.
     */
//    @GetMapping("/chatbot-segment-data/{id}")
//    public ResponseEntity<ChatbotSegmentData> getChatbotSegmentData(@PathVariable Long id) {
//        log.debug("REST request to get ChatbotSegmentData : {}", id);
//        Optional<ChatbotSegmentData> chatbotSegmentData = chatbotSegmentDataService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(chatbotSegmentData);
//    }
    
    @GetMapping("/chatbot-segment-data/{masterKey}")
    public ResponseEntity<ChatbotSegmentData> getChatbotSegmentData(@PathVariable String masterKey) {
        log.debug("REST request to get ChatbotSegmentData : {}", masterKey);
        Optional<ChatbotSegmentData> chatbotSegmentData = chatbotSegmentDataService.findOneByMasterKey(masterKey);
        return ResponseUtil.wrapOrNotFound(chatbotSegmentData);
    }


    /**
     * {@code DELETE  /chatbot-segment-data/:id} : delete the "id" chatbotSegmentData.
     *
     * @param id the id of the chatbotSegmentData to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chatbot-segment-data/{id}")
    public ResponseEntity<Void> deleteChatbotSegmentData(@PathVariable Long id) {
        log.debug("REST request to delete ChatbotSegmentData : {}", id);
        chatbotSegmentDataService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
    

    @GetMapping("/chat-svc/v1/chatbot/chat/{master-key}")
    public ResponseEntity<ChatBotInteractionNode> chat(ChatBotInteractionNode chatBotNodeMessage, ChatContext<String, String> chatContext, String masterKey) {
        ResponseEntity<ChatbotSegmentData> chatbotSegmentData = getChatbotSegmentData(masterKey);
        return null;
    }
}
