package co.seedwill.tech.chat.web.rest;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

//import com.example.messagingstompwebsocket.Greeting;
//import com.example.messagingstompwebsocket.HelloMessage;

@Controller
public class GreetingResource {


//	@MessageMapping("/hello")
//	@SendTo("/topic/greetings")
//	public Greeting greeting(HelloMessage message) throws Exception {
//		Thread.sleep(1000); // simulated delay
//		return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
//	}
	
	@MessageMapping("/hello2")
	@SendTo("/topic/greetings")
	public Integer greeting2(Integer message) throws Exception {
		Thread.sleep(1000); // simulated delay
		return message;
	}

}
