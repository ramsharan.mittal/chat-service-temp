package co.seedwill.tech.chat.web.rest;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import co.seedwill.tech.chat.domain.ChatBotInteractionNode;
import co.seedwill.tech.chat.domain.ChatContext;
import co.seedwill.tech.chat.domain.ChatbotSegmentData;
import co.seedwill.tech.chat.service.ChatBotMessageService;
import co.seedwill.tech.chat.service.ChatbotSegmentDataService;
import tech.jhipster.web.util.ResponseUtil;

@RestController
public class MyChatBotResource {
  private final Logger log = LoggerFactory.getLogger(ChatbotSegmentDataResource.class);
	@Autowired private ChatBotMessageService chatBotMessageService;

	@Autowired private ChatbotSegmentDataService chatbotSegmentDataService;

	@GetMapping("/guidedChat")
	public ResponseEntity<JsonNode> get() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode json = mapper.readTree("{\"id\": \"132\", \"name\": \"Alice\"}");
		return ResponseEntity.ok(json);
	}

	@GetMapping(path = "/guidedChat2")
	public String sayHello() {
		return "{\"id\": \"132\", \"name\": \"Alice\"}";
	}
	
	@GetMapping("/chatBot/greeting")
	public ResponseEntity<JsonNode> get2() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode json = mapper.readTree("{\"id\": \"132\", \"name\": \"Alice\"}");
		return ResponseEntity.ok(json);
	}
	
	@PostMapping("/chatbot/upload/chat")
	public ResponseEntity<ChatBotInteractionNode> chatBot(ChatBotInteractionNode chatBotNodeMessage) throws JsonProcessingException {
		ChatBotInteractionNode resultChatBotNodeMessage = null;
//		ObjectMapper mapper = new ObjectMapper();
//		JsonNode json = mapper.readTree("{\"id\": \"132\", \"name\": \"Alice\"}");
		resultChatBotNodeMessage = chatBotMessageService.save(chatBotNodeMessage);
		return ResponseEntity.ok(resultChatBotNodeMessage);
	}
	
	@GetMapping("/chat-svc/v1/chatbot/chat/{masterKey}")
    public ResponseEntity<ChatBotInteractionNode> chat(ChatBotInteractionNode chatBotNodeMessage, ChatContext<String, String> chatContext, @PathVariable String masterKey) throws JsonProcessingException {
//        ChatBotInteractionNode resultChatBotNodeMessage = null;
//      ObjectMapper mapper = new ObjectMapper();
//      JsonNode json = mapper.readTree("{\"id\": \"132\", \"name\": \"Alice\"}");
//        resultChatBotNodeMessage = chatBotMessageService.save(chatBotNodeMessage);
//        return ResponseEntity.ok(resultChatBotNodeMessage);
	  log.debug("REST request to get ChatbotSegmentData : {}", masterKey);
      Optional<ChatbotSegmentData> chatbotSegmentData = chatbotSegmentDataService.findOneByMasterKey(masterKey);
      return ResponseUtil.wrapOrNotFound(Optional.of(new ChatBotInteractionNode().fromString(chatbotSegmentData.get().getChat())));
    }

}