package co.seedwill.tech.chat.web.rest;

import co.seedwill.tech.chat.domain.ChatbotMasterData;
import co.seedwill.tech.chat.repository.ChatbotMasterDataRepository;
import co.seedwill.tech.chat.service.ChatbotMasterDataService;
import co.seedwill.tech.chat.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link co.seedwill.tech.chat.domain.ChatbotMasterData}.
 */
@RestController
@RequestMapping("/api")
public class ChatbotMasterDataResource {

    private final Logger log = LoggerFactory.getLogger(ChatbotMasterDataResource.class);

    private static final String ENTITY_NAME = "chatAppChatbotMasterData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChatbotMasterDataService chatbotMasterDataService;

    private final ChatbotMasterDataRepository chatbotMasterDataRepository;

    public ChatbotMasterDataResource(
        ChatbotMasterDataService chatbotMasterDataService,
        ChatbotMasterDataRepository chatbotMasterDataRepository
    ) {
        this.chatbotMasterDataService = chatbotMasterDataService;
        this.chatbotMasterDataRepository = chatbotMasterDataRepository;
    }

    /**
     * {@code POST  /chatbot-master-data} : Create a new chatbotMasterData.
     *
     * @param chatbotMasterData the chatbotMasterData to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chatbotMasterData, or with status {@code 400 (Bad Request)} if the chatbotMasterData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chatbot-master-data")
    public ResponseEntity<ChatbotMasterData> createChatbotMasterData(@RequestBody ChatbotMasterData chatbotMasterData)
        throws URISyntaxException {
        log.debug("REST request to save ChatbotMasterData : {}", chatbotMasterData);
        if (chatbotMasterData.getId() != null) {
            throw new BadRequestAlertException("A new chatbotMasterData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChatbotMasterData result = chatbotMasterDataService.save(chatbotMasterData);
        return ResponseEntity
            .created(new URI("/api/chatbot-master-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chatbot-master-data/:id} : Updates an existing chatbotMasterData.
     *
     * @param id the id of the chatbotMasterData to save.
     * @param chatbotMasterData the chatbotMasterData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chatbotMasterData,
     * or with status {@code 400 (Bad Request)} if the chatbotMasterData is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chatbotMasterData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chatbot-master-data/{id}")
    public ResponseEntity<ChatbotMasterData> updateChatbotMasterData(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChatbotMasterData chatbotMasterData
    ) throws URISyntaxException {
        log.debug("REST request to update ChatbotMasterData : {}, {}", id, chatbotMasterData);
        if (chatbotMasterData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chatbotMasterData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chatbotMasterDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChatbotMasterData result = chatbotMasterDataService.save(chatbotMasterData);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, chatbotMasterData.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /chatbot-master-data/:id} : Partial updates given fields of an existing chatbotMasterData, field will ignore if it is null
     *
     * @param id the id of the chatbotMasterData to save.
     * @param chatbotMasterData the chatbotMasterData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chatbotMasterData,
     * or with status {@code 400 (Bad Request)} if the chatbotMasterData is not valid,
     * or with status {@code 404 (Not Found)} if the chatbotMasterData is not found,
     * or with status {@code 500 (Internal Server Error)} if the chatbotMasterData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/chatbot-master-data/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ChatbotMasterData> partialUpdateChatbotMasterData(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChatbotMasterData chatbotMasterData
    ) throws URISyntaxException {
        log.debug("REST request to partial update ChatbotMasterData partially : {}, {}", id, chatbotMasterData);
        if (chatbotMasterData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chatbotMasterData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chatbotMasterDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChatbotMasterData> result = chatbotMasterDataService.partialUpdate(chatbotMasterData);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, chatbotMasterData.getId().toString())
        );
    }

    /**
     * {@code GET  /chatbot-master-data} : get all the chatbotMasterData.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chatbotMasterData in body.
     */
    @GetMapping("/chatbot-master-data")
    public List<ChatbotMasterData> getAllChatbotMasterData() {
        log.debug("REST request to get all ChatbotMasterData");
        return chatbotMasterDataService.findAll();
    }

    /**
     * {@code GET  /chatbot-master-data/:id} : get the "id" chatbotMasterData.
     *
     * @param id the id of the chatbotMasterData to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chatbotMasterData, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chatbot-master-data/{id}")
    public ResponseEntity<ChatbotMasterData> getChatbotMasterData(@PathVariable Long id) {
        log.debug("REST request to get ChatbotMasterData : {}", id);
        Optional<ChatbotMasterData> chatbotMasterData = chatbotMasterDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chatbotMasterData);
    }

    /**
     * {@code DELETE  /chatbot-master-data/:id} : delete the "id" chatbotMasterData.
     *
     * @param id the id of the chatbotMasterData to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chatbot-master-data/{id}")
    public ResponseEntity<Void> deleteChatbotMasterData(@PathVariable Long id) {
        log.debug("REST request to delete ChatbotMasterData : {}", id);
        chatbotMasterDataService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
